package cn.wubo.file.preview.exception;

public class PageRuntimeException extends RuntimeException{
    public PageRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
