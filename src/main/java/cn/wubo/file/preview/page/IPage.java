package cn.wubo.file.preview.page;

import org.springframework.web.servlet.function.ServerResponse;

public interface IPage {

    ServerResponse build();
}
